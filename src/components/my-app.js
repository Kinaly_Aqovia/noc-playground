/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html, css } from 'lit-element';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { installMediaQueryWatcher } from 'pwa-helpers/media-query.js';
import { installOfflineWatcher } from 'pwa-helpers/network.js';
import { installRouter } from 'pwa-helpers/router.js';
import { updateMetadata } from 'pwa-helpers/metadata.js';

// This element is connected to the Redux store.
import { store } from '../store.js';

// These are the actions needed by this element.
import {
  navigate,
  updateOffline,
  updateDrawerState
} from '../actions/app.js';

// These are the elements needed by this element.
import '@polymer/app-layout/app-drawer/app-drawer.js';
import { menuIcon } from './my-icons.js';
import './snack-bar.js';

class MyApp extends connect(store)(LitElement) {
  static get properties() {
    return {
      appTitle: { type: String },
      _page: { type: String },
      _drawerOpened: { type: Boolean },
      _snackbarOpened: { type: Boolean },
      _offline: { type: Boolean }
    };
  }

  static get styles() {
    return [
      css`
        :host {
          box-sizing: border-box;
          display: block;
          --app-drawer-width: 300px;
          --app-drawer-background-color: #005b7a;
        }

        * {
          box-sizing: inherit;
        }

        .app-wrapper {
          display: flex;
          flex-direction: column;
          height: 100vh;
        }

        header {
          flex: 0 0 auto;
          display: flex;
          flex-direction: column;
          align-items: center;
          background-color: #004568;
        }
        
        @media (min-width: 460px) {
          header {
            flex-direction: row;
          }
        }

        .page-content {
          flex: 1 1 auto;
          display: flex;
        }

        /* Workaround for IE11 displaying <main> as inline */
        main {
          display: block;
        }

        .main-content {
          padding: 1rem 1.5rem;
          background-color: #e8e8e8;
        }

        .main-sidebar {
          flex: 1 0 25%;
          min-width: 12em;
          max-width: 19em;

          background-color: var(--app-drawer-background-color);
        }

        .menu-btn {
          display: none;
        }

        @media (max-width: 900px) {
          .main-sidebar {
            display: none;
          }
          
          .menu-btn {
            display: inline-block;
          }
        }

        .drawer-nav {
          z-index: 2;
        }

        .main-nav {
          height: 100%;
          padding: .5rem .75rem;
          background: var(--app-drawer-background-color);
        }

        .main-nav ul {
          list-style-type: none;
          padding: 0;
          margin: 0;
        }

        .main-nav__item {
          border: 1px solid rgba(216, 216, 216, .3);
          border-width: 1px 0 0;
        }

        .main-nav__item:last-child {
          border-width: 1px 0;
        }

        .main-nav__item a {
          display: block;
          padding: .75rem 0;

          text-decoration: none;
          font-size: 1.125em;
          color: #fff;
        }


        .page {
          display: none;
        }

        .page[active] {
          display: block;
        }

        footer {
          flex: 0 0 auto;
          text-align: center;
        }
      `
    ];
  }

  render() {
    // Anything that's related to rendering should be done in here.
    return html`
      <!-- Drawer content -->
      <app-drawer
          .opened="${this._drawerOpened}"
          @opened-changed="${this._drawerOpenedChanged}" class="drawer-nav">
        <nav class="main-nav">
          <ul>
            <li class="main-nav__item">
              <a ?selected="${this._page === 'view1'}" href="/view1">View One</a>
            </li>

            <li class="main-nav__item">
              <a ?selected="${this._page === 'view2'}" href="/view2">View Two</a>
            </li>

            <li class="main-nav__item">
              <a ?selected="${this._page === 'view3'}" href="/view3">View Three</a>
            </li>
          </ul>
        </nav>
      </app-drawer>

      <div class="app-wrapper">
        <header>
          <button class="menu-btn" title="Menu" @click="${this._menuButtonClicked}">${menuIcon}</button>

          <p>${this.appTitle}</p>
        </header>
        
        <div class="page-content">

          <aside class="main-sidebar">
            <nav class="main-nav">
              <ul>
                <li class="main-nav__item">
                  <a ?selected="${this._page === 'view1'}" href="/view1">Cloud Services</a>
                </li>

                <li class="main-nav__item">
                  <a ?selected="${this._page === 'view2'}" href="/view2">View Two</a>
                </li>

                <li class="main-nav__item">
                  <a ?selected="${this._page === 'view3'}" href="/view3">View Three</a>
                </li>
              </ul>
            </nav>
          </aside>

          <!-- Main content -->
          <main role="main" class="main-content">
            <my-view1 class="page" ?active="${this._page === 'view1'}"></my-view1>
            <my-view2 class="page" ?active="${this._page === 'view2'}"></my-view2>
            <my-view3 class="page" ?active="${this._page === 'view3'}"></my-view3>
            <my-view404 class="page" ?active="${this._page === 'view404'}"></my-view404>
          </main>
        </div>

        <footer>
          <p>Made with &hearts; by the Aqovia crew.</p>
        </footer>
      </div>

      <snack-bar ?active="${this._snackbarOpened}">
        You are now ${this._offline ? 'offline' : 'online'}.
      </snack-bar>
    `;
  }

  constructor() {
    super();
    // To force all event listeners for gestures to be passive.
    // See https://www.polymer-project.org/3.0/docs/devguide/settings#setting-passive-touch-gestures
    setPassiveTouchGestures(true);
  }

  firstUpdated() {
    installRouter((location) => store.dispatch(navigate(decodeURIComponent(location.pathname))));
    installOfflineWatcher((offline) => store.dispatch(updateOffline(offline)));
    installMediaQueryWatcher(`(min-width: 460px)`,
        () => store.dispatch(updateDrawerState(false)));
  }

  updated(changedProps) {
    if (changedProps.has('_page')) {
      const pageTitle = this.appTitle + ' - ' + this._page;
      updateMetadata({
        title: pageTitle,
        description: pageTitle
        // This object also takes an image property, that points to an img src.
      });
    }
  }

  _menuButtonClicked() {
    console.log("clicked");
    store.dispatch(updateDrawerState(true));
  }

  _drawerOpenedChanged(e) {
    store.dispatch(updateDrawerState(e.target.opened));
  }

  stateChanged(state) {
    this._page = state.app.page;
    this._offline = state.app.offline;
    this._snackbarOpened = state.app.snackbarOpened;
    this._drawerOpened = state.app.drawerOpened;
  }
}

window.customElements.define('my-app', MyApp);
